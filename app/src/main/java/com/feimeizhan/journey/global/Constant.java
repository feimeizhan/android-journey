package com.feimeizhan.journey.global;

/**
 * Created by Administrator on 2015/10/20.
 */
public class Constant {
    // 测试传递的各种值,比如:context,bundle
    public final static String CHECK_TAG = "Check";

    // fragment对应字符串
    public final static String MAIN_FRAGMENT_TAG = "MainFragment";
    public final static String CURSOR_LOADER_LIST_FRAGMENT_TAG = "CursorLoaderListFragment";
    public final static String FILES_FRAGMENT_TAG = "FilesFragment";
}
