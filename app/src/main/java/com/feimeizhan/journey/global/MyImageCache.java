package com.feimeizhan.journey.global;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.toolbox.ImageLoader;

/**
 * Created by mengwen on 2015/12/3.
 */
public class MyImageCache implements ImageLoader.ImageCache {

    private final static int DEF_MAX_SIZE = 1024;

    private LruCache<String, Bitmap> lruCache;

    public MyImageCache() {
        this(DEF_MAX_SIZE);
    }

    public MyImageCache(int maxSize) {
        lruCache = new LruCache<String, Bitmap>(maxSize);
    }

    @Override
    public Bitmap getBitmap(String url) {
        return lruCache.get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        lruCache.put(url, bitmap);
    }
}
