package com.feimeizhan.journey.global;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by mengwen on 2015/12/2.
 */
public class NetSingleton {

    public static final String TAG = "NetSingleton";

    public static final String DEF_REQ_TAG = "Default_Req_TAG"; // 默认的请求标识

    private RequestQueue mQueue;

    private ImageLoader imageLoader;

    private ImageLoader.ImageCache imageCache;

    private static NetSingleton mInstance;

    private NetSingleton(Context context) {

        mQueue = getRequestQueue(context);

        imageCache = new MyImageCache();

        imageLoader = new ImageLoader(mQueue, imageCache);
    }

    public static NetSingleton getInstance(Context context) {

        if (mInstance == null) {
            mInstance = new NetSingleton(context);
        }

        return mInstance;
    }

    private RequestQueue getRequestQueue(Context context) {

        return Volley.newRequestQueue(context);
    }

    public <T> void addToRequestQueue(Request<T> request) {
        addToRequestQueue(request, null);
    }

    /**
     * 如果tag为空，就用默认的请求标识DEF_REQ_TAG
     * @param request
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> request,@Nullable String tag) {
        request.setTag(TextUtils.isEmpty(tag) ? DEF_REQ_TAG : tag);

        mQueue.add(request);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }
}
