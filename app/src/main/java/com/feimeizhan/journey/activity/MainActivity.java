package com.feimeizhan.journey.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.feimeizhan.journey.R;
import com.feimeizhan.journey.fragment.CursorLoaderListFragment;
import com.feimeizhan.journey.fragment.FilesFragment;
import com.feimeizhan.journey.fragment.MainFragment;
import com.feimeizhan.journey.global.Constant;


public class MainActivity extends AppCompatActivity {

    private FragmentTransaction ft;
    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fm = getFragmentManager();
        /**
         * 一个事务只能有一个commit
         * commit之后要beginTransaction()后才能再commit
         */
        // ft = fm.beginTransaction();

        changeFragment(Constant.MAIN_FRAGMENT_TAG);

    }

    /**
     * 通过传递的字符串,切换相应fragment
     *
     * @param str 要切换fragment的字符串
     */
    public void changeFragment(String str) {

        ft = fm.beginTransaction();

        if (fm.findFragmentByTag(str) != null) {
            // 如果已有了,直接显示
            ft.show(fm.findFragmentByTag(str));
        } else {
            switch (str) {
                case Constant.MAIN_FRAGMENT_TAG:
                    /**
                     * 如果是add()就只是添加view,如果onCreateView()返回的view不是null
                     * replace()是先remove(),然后add()
                     */
                    // ft.add(new MainFragment(), Constant.MAIN_FRAGMENT_TAG);
                    ft.replace(R.id.main_container, new MainFragment(), Constant.MAIN_FRAGMENT_TAG);
                    break;
                case Constant.CURSOR_LOADER_LIST_FRAGMENT_TAG:
                    ft.replace(R.id.main_container, new CursorLoaderListFragment(), Constant.CURSOR_LOADER_LIST_FRAGMENT_TAG);
                    break;
                case Constant.FILES_FRAGMENT_TAG:
                    ft.replace(R.id.main_container, new FilesFragment(), Constant.FILES_FRAGMENT_TAG);
                    break;
            }
        }
        ft.commit();
    }
}
