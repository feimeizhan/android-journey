package com.feimeizhan.journey.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.feimeizhan.journey.R;
import com.feimeizhan.journey.activity.MainActivity;
import com.feimeizhan.journey.global.Constant;


public class MainFragment extends Fragment implements View.OnClickListener{

    private Button cursorLoaderBtn;
    private Button filesBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, null);
        cursorLoaderBtn = (Button) view.findViewById(R.id.cursor_loader_btn);
        filesBtn = (Button) view.findViewById(R.id.files_btn);
        cursorLoaderBtn.setOnClickListener(this);
        filesBtn.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cursor_loader_btn:
                ((MainActivity)getActivity()).changeFragment(Constant.CURSOR_LOADER_LIST_FRAGMENT_TAG);
                break;
            case R.id.files_btn:
                ((MainActivity) getActivity()).changeFragment(Constant.FILES_FRAGMENT_TAG);
                break;
        }
    }
}
