package com.feimeizhan.journey.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.feimeizhan.journey.R;
import com.feimeizhan.journey.config.FilesConfig;
import com.feimeizhan.journey.global.Constant;
import com.feimeizhan.journey.global.NetSingleton;
import com.feimeizhan.journey.utils.ImageUtil;

import java.io.IOException;

/**
 * Created by mengwen on 2015/12/2.
 */
public class FilesFragment extends Fragment implements View.OnClickListener {

    private NetworkImageView downloadImageView;
    private ImageView downloadNormalImageView;
    private Button saveImgBtn;
    private ToggleButton scanToggleBtn;

    private Bitmap bitmap; // 下载图片的bitmap
    private boolean isDownLoaded = false; // 是否下载图片成功
    private boolean isScanned = true; // 是否扫描

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_files, container, false);

        downloadImageView = (NetworkImageView) view.findViewById(R.id.download_iv);
        downloadNormalImageView = (ImageView) view.findViewById(R.id.download_normal_iv);
        scanToggleBtn = (ToggleButton) view.findViewById(R.id.scan_toggle_btn);
        view.findViewById(R.id.download_img_btn).setOnClickListener(this);
        view.findViewById(R.id.open_gallery_btn).setOnClickListener(this);
        saveImgBtn = (Button) view.findViewById(R.id.save_img_btn);
        saveImgBtn.setOnClickListener(this);
        saveImgBtn.setClickable(isDownLoaded);

        scanToggleBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isScanned = true;
                } else {
                    isScanned = false;
                }
            }
        });

        scanToggleBtn.setChecked(isScanned);

        return view;
    }


    private void downloadImage() {

        String imgUrl = "https://s-media-cache-ak0.pinimg.com/236x/b2/8b/1d/b28b1d8ac68b0756dece8fbe98268386.jpg";

//        ImageLoader imageLoader = NetSingleton.getInstance(this.getActivity()).getImageLoader();
        downloadImageView.setDefaultImageResId(R.drawable.ic_image_black_48dp);
//        downloadImageView.setErrorImageResId(R.drawable.ic_broken_image_black_48dp);
//        downloadImageView.setImageUrl(imgUrl, imageLoader);

        ImageRequest request = new ImageRequest(
                imgUrl,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        bitmap = response;
                        downloadNormalImageView.setImageBitmap(bitmap);
                        isDownLoaded = true;
                        saveImgBtn.setClickable(isDownLoaded);
                    }
                }, 200, 200, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.RGB_565,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(Constant.FILES_FRAGMENT_TAG, error.toString());
                        downloadNormalImageView.setImageResource(R.drawable.ic_broken_image_black_48dp);

                    }
                }
        );

        NetSingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.download_img_btn:
                downloadImage();
                break;
            case R.id.open_gallery_btn:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivity(intent);
                break;
            case R.id.save_img_btn:
                try {
                    ImageUtil.saveBitmapToSDCard(this.getActivity(), bitmap, FilesConfig.IMG_DIR_NAME, Bitmap.CompressFormat.PNG, 100, isScanned);
                    Toast.makeText(this.getActivity(), "Save successfully", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(Constant.FILES_FRAGMENT_TAG, "save image file error");
                }
                break;
            default:
                break;
        }
    }
}
