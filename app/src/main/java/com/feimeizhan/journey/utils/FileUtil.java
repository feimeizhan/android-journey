package com.feimeizhan.journey.utils;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by mengwen on 2015/12/2.
 */
public class FileUtil {

    private static final String TAG = "FileUtil";

    /**
     * 扫描一系列文件
     * @param context
     * @param paths
     * @param mimeTypes
     * @param completedListener
     */
    public static void scanFiles(Context context, String[] paths, String[] mimeTypes,@Nullable MediaScannerConnection.OnScanCompletedListener completedListener) {

        // 判断监听器是否为null
        // true：新建默认监听器
        // false：使用该监听器

        if (completedListener == null) {
            completedListener = new MediaScannerConnection.OnScanCompletedListener() {
                @Override
                public void onScanCompleted(String path, Uri uri) {
                    Log.e(TAG, "Scanner completed");
                    Log.e(TAG, "Uri:" + String.valueOf(uri));
                    Log.e(TAG, "Path:" + String.valueOf(path));
                }
            };
        } else {

        }

        MediaScannerConnection.scanFile(context, paths, mimeTypes, completedListener);
    }
}
