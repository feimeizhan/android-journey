package com.feimeizhan.journey.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by mengwen on 2015/12/3.
 */
public class ImageUtil {

    private static final String TAG = "ImageUtil";

    /**
     * 从bitmap获取bytes
     * @param context
     * @param bitmap
     * @param format
     * @param quality
     * @return
     * @throws IOException
     */
    public static byte[] getBytesFromBitmap(Context context,
                                          Bitmap bitmap,
                                          Bitmap.CompressFormat format,
                                          int quality)
            throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bitmap.compress(format, quality, out);
        out.flush();
        out.close();

        return out.toByteArray();
    }


    /**
     * TODO
     * @param context
     * @param bitmap
     */
    public static void saveBitmapToInternalStorage(Context context, Bitmap bitmap) {

    }

    /**
     * 保存bitmap到文件路径path下
     * @param context
     * @param bitmap
     * @param dirName 保存的文件夹名称
     * @param format 保存格式
     * @param quality 保存的质量，0-100
     * @param isScanned 是否可以被系统图库扫描
     * @throws IOException
     */
    public static void saveBitmapToSDCard(final Context context,
                                          Bitmap bitmap,
                                          String dirName,
                                          Bitmap.CompressFormat format,
                                          int quality,
                                          boolean isScanned)
            throws IOException {


        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            String fileName = System.currentTimeMillis() + "." + format.name();
            File dir = new File(Environment.getExternalStorageDirectory(), dirName);

            if (!dir.exists() || !dir.isDirectory()) {
                dir.mkdirs();
            }

            File file = new File(dir, fileName);

            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));

            out.write(getBytesFromBitmap(context, bitmap, format, quality));
            out.close();

            if (isScanned) {
                MediaScannerConnection.scanFile(context, new String[]{file.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        Log.e(TAG, "Scan successfully");
                        Log.e(TAG, "path:" + path);
                    }
                });
//                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//                Uri uri = Uri.fromFile(file);
//                intent.setData(uri);
//                context.sendBroadcast(intent);
            }

        } else {
            throw new IOException("The SDCard is not mounted or available.");
        }
    }
}
