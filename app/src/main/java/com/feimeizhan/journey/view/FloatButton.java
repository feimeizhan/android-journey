package com.feimeizhan.journey.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.feimeizhan.journey.R;

/**
 * be similar with android 5.0 add button
 */
public class FloatButton extends View {
    private int mFloatColor = Color.RED;
    private float mFloatDimension = 0;
    private Drawable mFloatDrawable;

    public FloatButton(Context context) {
        super(context);
        init(null, 0);
    }

    public FloatButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public FloatButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.FloatButton, defStyle, 0);

        mFloatColor = a.getColor(
                R.styleable.FloatButton_FloatColor,
                mFloatColor);
        // Use getDimensionPixelSize or getDimensionPixelOffset when dealing with
        // values that should fall on pixel boundaries.
        mFloatDimension = a.getDimension(
                R.styleable.FloatButton_FloatDimension,
                mFloatDimension);

        if (a.hasValue(R.styleable.FloatButton_FloatDrawable)) {
            mFloatDrawable = a.getDrawable(
                    R.styleable.FloatButton_FloatDrawable);
            mFloatDrawable.setCallback(this);
        }

        a.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // TODO: consider storing these as member variables to reduce
        // allocations per draw cycle.
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

        // Draw the drawable on top of the text.
        if (mFloatDrawable != null) {
            mFloatDrawable.setBounds(paddingLeft, paddingTop,
                    paddingLeft + contentWidth, paddingTop + contentHeight);
            mFloatDrawable.draw(canvas);
        }
    }

    /**
     * Gets the color attribute value.
     *
     * @return The color attribute value.
     */
    public int getFloatColor() {
        return mFloatColor;
    }

    /**
     * Sets the view's color attribute value. In the view, this color
     * is the font color.
     *
     * @param floatColor The color attribute value to use.
     */
    public void setFloatColor(int floatColor) {
        mFloatColor = floatColor;
    }

    /**
     * Gets the dimension attribute value.
     *
     * @return The dimension attribute value.
     */
    public float getFloatDimension() {
        return mFloatDimension;
    }

    /**
     * Sets the view's dimension attribute value. In the view, this dimension
     * is the font size.
     *
     * @param floatDimension The dimension attribute value to use.
     */
    public void setFloatDimension(float floatDimension) {
        mFloatDimension = floatDimension;
    }

    /**
     * Gets the drawable attribute value.
     *
     * @return The drawable attribute value.
     */
    public Drawable getFloatDrawable() {
        return mFloatDrawable;
    }

    /**
     * Sets the view's drawable attribute value. In the view, this drawable is
     * drawn above the text.
     *
     * @param floatDrawable The drawable attribute value to use.
     */
    public void setFloatDrawable(Drawable floatDrawable) {
        mFloatDrawable = floatDrawable;
    }
}
